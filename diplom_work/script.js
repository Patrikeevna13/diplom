'use strict'

const form = document.querySelector('form');
const USERNAME = 'patrikeevna13@yandex.ru';
const PASSWORD = 'password13';
checkAuth();

if (form) {
	form.addEventListener('submit', (e) => {
		formSubmit(e);
	});
}

function checkAuth() {
	const locationHref = window.location.href;
	const currentPage = locationHref.substr(locationHref.lastIndexOf('/'), locationHref.length);
	const username = localStorage.getItem('username');
	const password = localStorage.getItem('password');

	if (username !== USERNAME && password !== PASSWORD && currentPage !== '/index.html') {
		window.location.href = 'index.html';
	}
}

function formSubmit(e) {
	e.preventDefault();
	let errorEmailIncorrect = false;
	let errorEmail = false;
	let errorEmailValid = false;
	let errorPassword = false;
	let errorPasswordIncorrect = false;
	let errorCheckbox = false;
	let errorPasswordLenght = false;
	let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	const inputEmail = form.querySelector('#email-form');
	const messageEmail = form.querySelector('.error_email');
	const messageEmailInvalid = form.querySelector('.error_email_unvalid');
	const messageEmailIncorrect = form.querySelector('.error_email_incorrect');
	const messagePassword = form.querySelector('.error_password');
	const messagePasswordShort = form.querySelector('.error_password_symbol');
	const messagePasswordIncorrect = form.querySelector('.error_password_incorrect');
	const inputPassword = form.querySelector('#password-form');
	const messageCheckbox = form.querySelector('.error_checkbox');
	const inputCheckbox = form.querySelector('.form__checkbox');
	const borderCheckbox = form.querySelector('.form__checkbox-mark');
	const redStarEmail = form.querySelector('#star_email');
	const redStarPassword = form.querySelector('#star_password');
	const redStarCheckbox = form.querySelector('#star_checkbox');
	let email = inputEmail.value;
	let password = inputPassword.value;

	errorEmail = inputEmail.value === '';
	errorPassword = inputPassword.value === '';
	errorPasswordLenght = inputPassword.value.length < 8;
	errorCheckbox = inputCheckbox.checked === false;
	errorEmailValid = inputEmail.value.match(re);

	if (errorEmail) {
		e.preventDefault();
		inputEmail.style.border = '1px solid red';
		messageEmail.style.display = 'block';
		messageEmailIncorrect.style.display = 'none';
		messageEmailInvalid.style.display = 'none';
		redStarEmail.style.color = 'red';
	}
	else if (!errorEmailValid) {
		e.preventDefault();
		inputEmail.style.border = '1px solid red';
		messageEmailInvalid.style.display = 'block';
		redStarEmail.style.color = 'red';
		messageEmailIncorrect.style.display = 'none';
		messageEmail.style.display = 'none';
	}
	else if (email !== USERNAME) {
		e.preventDefault();
		inputEmail.style.border = '1px solid red';
		messageEmailIncorrect.style.display = 'block';
		messageEmailInvalid.style.display = 'none';
		messageEmail.style.display = 'none';
		redStarEmail.style.color = 'red';
	}
	else {
		inputEmail.style.border = '1px solid black';
		messageEmailInvalid.style.display = 'none';
		messageEmail.style.display = 'none';
		messageEmailIncorrect.style.display = 'none';
		redStarEmail.style.color = 'black';
	}

	if (errorPassword) {
		e.preventDefault();
		inputPassword.style.border = '1px solid red';
		messagePassword.style.display = 'block';
		messagePasswordShort.style.display = 'none';
		messagePasswordIncorrect.style.display = 'none';
		redStarPassword.style.color = 'red';
	}
	else if (errorPasswordLenght) {
		e.preventDefault();
		inputPassword.style.border = '1px solid red';
		messagePasswordShort.style.display = 'block';
		messagePassword.style.display = 'none';
		messagePasswordIncorrect.style.display = 'none';
		redStarPassword.style.color = 'red';
	}
	else if (password !== PASSWORD) {
		e.preventDefault();
		inputPassword.style.border = '1px solid red';
		messagePasswordIncorrect.style.display = 'block';
		messagePassword.style.display = 'none';
		messagePasswordShort.style.display = 'none'
		redStarPassword.style.color = 'red';
	}
	else {
		inputPassword.style.border = '1px solid black';
		messagePasswordShort.style.display = 'none';
		messagePassword.style.display = 'none';
		messagePasswordIncorrect.style.display = 'none';
		redStarPassword.style.color = 'black';
	}

	if (errorCheckbox) {
		e.preventDefault()
		borderCheckbox.style.border = '2px solid red';
		messageCheckbox.style.display = 'block';
		redStarCheckbox.style.color = 'red';
	}
	else {
		borderCheckbox.style.border = '2px solid black';
		messageCheckbox.style.display = 'none';
		redStarCheckbox.style.color = 'black';
	}

	if (!errorEmail && errorEmailValid && !errorPassword && !errorPasswordLenght && !errorCheckbox) {
		localStorage.setItem('username', email);
		localStorage.setItem('password', password);
		window.location = 'category.html';
	}
}

class Cart {
	constructor() {
		this.localStorageId = 'cart';
		this.cartStore = this.getFromLocalStorage();
		this.basketList = document.querySelector('.js-basket-list');
		this.cartCount = document.querySelector('.js-cart-count');
		this.cartSum = document.querySelector('.js-cart-sum');

		this.setListeners();
		this.renderProducts();
		this.setCount();
		this.setSum();
	}
	
	setListeners() {
		window.addEventListener('storage', () => {
			this.cartStore = this.getFromLocalStorage();
			this.renderProducts();
			this.setCount();
			this.setSum();
		})
	}

	addToStore(product) {
		this.cartStore.push(product);

		this.saveToLocalStorage();
		this.setCount();
		this.setSum();
	}

	removeFromStore(productName){
		this.cartStore = this.cartStore.filter((product) => product.title !== productName);

		this.renderProducts();
		this.saveToLocalStorage();
		this.setCount();
		this.setSum();
	}

	saveToLocalStorage() {
		const cartStoreStringify = JSON.stringify(this.cartStore);
		localStorage.setItem(this.localStorageId, cartStoreStringify);
	}

	getFromLocalStorage() {
		const cartStoreStringify = localStorage.getItem(this.localStorageId);

		if (cartStoreStringify) {
			return JSON.parse(cartStoreStringify);
		}
		return [];
	}

	renderProducts() {
		if (!this.basketList) {
			return;
		}

		this.basketList.innerHTML = '';

		this.cartStore.forEach((product) => {
			this.basketList.append(this.getProductHtml(product));
		});
	}

	getProductHtml(product) {
		const basketProduct = document.createElement('div');
		basketProduct.classList.add('basket__product', 'js-product');
		
		const basketProductImg = document.createElement('img');
		basketProductImg.classList.add('basket__product-img', 'js-product-img');
		basketProductImg.src = product.image;

		const basketProductTitle = document.createElement('span');
		basketProductTitle.classList.add('basket__product-title', 'js-product-title');
		basketProductTitle.textContent = product.title;

		const basketProductRight = document.createElement('div');
		basketProductRight.classList.add('basket__product-right');

		const basketSum = document.createElement('span');
		basketSum.classList.add('basket__summ', 'js-product-price');
		basketSum.textContent = product.price;

		const basketCurrency = document.createElement('span');
		basketCurrency.classList.add('basket__summ');
		basketCurrency.textContent = ' ₽';

		const basketButton = document.createElement('button');
		basketButton.classList.add('basket__button', 'basket__button-close', 'js-product-remove');
		basketButton.addEventListener('click', () => {
			this.removeFromStore(product.title);
		});
		
		basketProductRight.append(basketSum);
		basketProductRight.append(basketCurrency);
		basketProductRight.append(basketButton);
		basketProduct.append(basketProductImg);
		basketProduct.append(basketProductTitle);
		basketProduct.append(basketProductRight);
		return basketProduct;
	}

	getCount() {
		return this.cartStore.length;
	}

	getSum() {
		return this.cartStore.reduce((a, b) => (a + b.price), 0)
	}

	setCount() {
		if (!this.cartCount) {
			return;
		}
		this.cartCount.innerText = this.getCount();
	}

	setSum() {
		if (!this.cartSum) {
			return;
		}
		this.cartSum.innerText = this.getSum();
	}
}

window.cart = new Cart();

class Product {
	constructor(product) {
		this.product = product;
		this.title = this.product.querySelector('.js-product-title').textContent;
		this.price = parseInt(this.product.querySelector('.js-product-price').textContent);
		this.image = this.product.querySelector('.js-product-img').src;
		this.buyButton = this.product.querySelector('.js-product-buy-button');

		this.setListeners();
	}

	setListeners() {
		if (this.buyButton) {
			this.buyButton.addEventListener('click', () => {
				this.clickBuyBottom();
			});
		}
	}

	clickBuyBottom() {
		window.cart.addToStore({
			title: this.title,
			price: this.price,
			image: this.image,
		});
	}
}

document.querySelectorAll('.js-product').forEach((product) => {
	new Product(product);
});
